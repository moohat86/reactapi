const express = require('express')
const users = express.Router()
const cors = require('cors');
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
var md5 = require('md5');

const clientModel = require('../models/clients')
// const User = require('../models/user')

// users.use(cors())

process.env.SECRET_KEY = 'secret';
module.exports = {

    getAll: function (req, res, next) {
        let productsList = [];
        clientModel.findAll({}).then((result) => {
            for (let product of result) {
                productsList.push({ id: product.id, username: product.username, password: product.pin  });
            }
            res.json({ status: "success", message: "products list found!!!", data: { products: productsList } });
        }).catch((err) => {
            next(err);
        });
    },

   


     login: function (req, res, next) {
    
        clientModel.findOne({
                    where: {
                        username: req.body.username
                    }
                })
                .then((user) => {
                    if(user){
                        if((md5(req.body.pin) === user.pin)){
                        // if(bcrypt.compareSync(req.body.pin, user.pin)){ //--> jika pake bcrypt
                            let token = jwt.sign(user.dataValues, process.env.SECRET_KEY,{
                                expiresIn: 1440
                            })
                            // res.send(token)
            
                           res.json({ status: "success", message: "user found!!!", data: { username: user.username, token: token } });
            
                        }
                        else {
                            res.json({ status: "error", message: "Invalid pin!!!", data: null });
                         }
                    }else{
                        res.status(400).json({error: 'User Does not exist'})
                    }
                }).catch((err) => {
                    res.status(400).json({error: err})
                });
     },
}

