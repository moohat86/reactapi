const express = require('express')

const inquiryModel = require('../models/inquiry')
// const User = require('../models/user')

// users.use(cors())

process.env.SECRET_KEY = 'secret';
module.exports = {

    getAll: function (req, res, next) {
        let productsList = [];
        inquiryModel.findAll({}).then((result) => {
            for (let product of result) {
                productsList.push({ products: product  });
            }
            res.json({ status: "success", message: "products list found!!!", data: { products: productsList } });
        }).catch((err) => {
            next(err);
        });
    },


    getById: function (req, res, next) {
        inquiryModel.findByPk(req.params.id).then((result) => {
            if(result){
                res.json({ status: "success", message: "product found!!!", data: { products: result } });
            }else{
                res.json({message: "product Does not found!!!"});
            }
        }).catch((err) => {
            next(err);
        });
    },
    
    
    create: function (req, res, next) {        
        const inquiryData = {
            id_clients: req.body.id_clients,
            email: req.body.email,
            status: req.body.status,
            event_date: req.body.event_date,
            video: req.body.video,
            photo: req.body.photo,
            photobooth: req.body.photobooth,
            budget: req.body.budget,
            is_raw: req.body.is_raw,
            qty_photographer: req.body.qty_photographer,
            is_esession: req.body.is_esession,
            is_10_10_album: req.body.is_10_10_album,
            is_16x24_canvas: req.body.is_16x24_canvas,
            qty_videographer: req.body.qty_videographer,
            is_drone: req.body.is_drone,
            is_highlight: req.body.is_highlight,
            is_documentary: req.body.is_documentary,
            photobooth_style: req.body.photobooth_style,
        }
        inquiryModel.findOne({
            where: {
                id_clients: req.body.id_clients,
            }
        }).then((result) => {
            inquiryModel.create(inquiryData).then((result) => {
                res.json({ status: "success", message: "product added successfully!!!", data: { result } });

            }).catch((err) => {
                res.send({ error: err })
            });
        }).catch((err) => {
            res.send({ error: err })
        });
    },

    updateById: function (req, res, next) {
        const inquiryData = {
            id_clients: req.body.id_clients,
            email: req.body.email,
            status: req.body.status,
            event_date: req.body.event_date,
            video: req.body.video,
            photo: req.body.photo,
            photobooth: req.body.photobooth,
            budget: req.body.budget,
            is_raw: req.body.is_raw,
            qty_photographer: req.body.qty_photographer,
            is_esession: req.body.is_esession,
            is_10_10_album: req.body.is_10_10_album,
            is_16x24_canvas: req.body.is_16x24_canvas,
            qty_videographer: req.body.qty_videographer,
            is_drone: req.body.is_drone,
            is_highlight: req.body.is_highlight,
            is_documentary: req.body.is_documentary,
            photobooth_style: req.body.photobooth_style,
        }
        inquiryModel.findByPk(req.params.id).then((result) => {
            if(result){

                result.update(inquiryData).then((result) => {
                    res.json({ status: "success", message: "product updated successfully!!!", data: result });
    
                }).catch((err) => {
                    next(err);
    
                });
            }else{
                res.json({message: "product Does not found!!!"});

            }
        }).catch((err) => {
            next(err);

        });
    },

    deleteById: function (req, res, next) {
        inquiryModel.findOne({
            where: {
                id: req.params.id,
            }
        }).then((result) => {
            if(result){
                inquiryModel.destroy({
                    where: {
                        id: req.params.id,        
                    }
                }).then((result) => {
                    res.json({ status: "success", message: "product deleted successfully!!!", data: { result } });    
                }).catch((err) => {
                    res.send({ error: err })    
                });
            }else{
                res.json({error: err})
            }
        }).catch((err) => {
            res.send({ error: `data does not exist` })
        });
    },


  
}

