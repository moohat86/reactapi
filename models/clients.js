const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'client',
    {
        id:{
            type : Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        username:{
            type: Sequelize.STRING
        },
        role:{
            type: Sequelize.INTEGER
        },
        pin:{
            type: Sequelize.STRING
        }
    },
    {
        timestamps: false
    }
)