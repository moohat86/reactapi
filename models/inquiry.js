const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'inquiry',
    {
        id:{
            type : Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_clients:{
            type: Sequelize.INTEGER
        },
        email:{
            type: Sequelize.STRING
        },
        status:{
            type: Sequelize.INTEGER,
            defaultValue:0
        },
        event_date:{
            type: Sequelize.DATE
        },
        video:{
            type: Sequelize.INTEGER,
        },
        photo:{
            type: Sequelize.INTEGER,
        },
        photobooth:{
            type: Sequelize.INTEGER
        },
        budget:{
            type: Sequelize.DOUBLE
        },
        is_raw:{
            type: Sequelize.INTEGER
        },
        qty_photographer:{
            type: Sequelize.INTEGER
        },
        is_esession:{
            type: Sequelize.INTEGER
        },
        is_10_10_album:{
            type: Sequelize.INTEGER
        },
        is_16x24_canvas:{
            type: Sequelize.INTEGER
        },
        qty_videographer:{
            type: Sequelize.INTEGER
        },
        is_drone:{
            type: Sequelize.INTEGER
        },
        is_highlight:{
            type: Sequelize.INTEGER
        },
        is_documentary:{
            type: Sequelize.INTEGER
        },
        photobooth_style:{
            type: Sequelize.INTEGER
        },
        created_date:{
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW
        }
    },
    {
        timestamps: false,
        freezeTableName: true,
        
    }
)