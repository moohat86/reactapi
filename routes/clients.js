const express = require('express');
const router = express.Router();
var http = require("http");
const clientController = require('../controllers/clients');
// router.post('/register', clientController.register);
router.post('/login', clientController.login);
router.get('/', clientController.getAll);
module.exports = router;