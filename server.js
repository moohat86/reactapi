var express = require('express');
var cors = require('cors')
var bodyParser = require('body-parser');
var app = express();


app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));


var clients = require('./routes/clients');
var inquiry = require('./routes/inquiry');


app.use('/api/clients', clients);
app.use('/api/inquiries', inquiry);


// const Sequelize = require('sequelize');


// App Server Connection
app.listen(process.env.PORT || 3000, () => {
    console.log(`aplikasi running pada port ${process.env.PORT || 3000}`)
  })
